import pgzero
from util import *

def setup_scene(renderer):
    """Setup the scene. Mostly the GameWorld Damn this is ugly but damn it just works
    
    Arguments:
        renderer {GraphicsManager} -- We need the renderer in here to manage the global graphics
    """
    top_left = 500
    top_right = 780
    bottom_left = 235
    bottom_right = 470

    renderer.generate_gameobject("roadside", Vector2(top_left,0), Vector2(5, 2000), (200,200,200))
    renderer.generate_gameobject("roadside", Vector2(top_right,0), Vector2(5, 2000), (200,200,200))
    renderer.generate_gameobject("roadside", Vector2(0,bottom_left), Vector2(2000, 5), (200,200,200))
    renderer.generate_gameobject("roadside", Vector2(0,bottom_right), Vector2(2000, 5), (200,200,200))

    renderer.generate_gameobject("roadside", Vector2((top_left + top_right) / 2 ,0), Vector2(5, 2000), (200,200,200))
    renderer.generate_gameobject("roadside", Vector2(0, (bottom_left + bottom_right) / 2), Vector2(2000, 5), (200,200,200))

    dash_index = top_right
    while dash_index < WIDTH:
        renderer.generate_gameobject("roadside",Vector2(dash_index,((bottom_left + bottom_right) / 2) - 10), Vector2(20, 20), (0,0,0))
        dash_index += 35

    dash_index = top_left
    while dash_index > 0:
        renderer.generate_gameobject("roadside",Vector2(dash_index,((bottom_left + bottom_right) / 2) - 10), Vector2(20, 20), (0,0,0))
        dash_index -= 35

    dash_index = bottom_right
    while dash_index < HEIGHT:
        renderer.generate_gameobject("roadside",Vector2(((top_left + top_right) / 2) - 10, dash_index), Vector2(20, 20), (0,0,0))
        dash_index += 35

    dash_index = bottom_left
    while dash_index > 0:
        renderer.generate_gameobject("roadside",Vector2(((top_left + top_right) / 2) - 10, dash_index), Vector2(20, 20), (0,0,0))
        dash_index -= 35

    dash_index = bottom_left + 10
    while dash_index < bottom_right:
        renderer.generate_gameobject("roadside",Vector2(top_left, dash_index - 5), Vector2(20, 20), (0,0,0))
        dash_index += 35

    dash_index = bottom_left + 10
    while dash_index < bottom_right:
        renderer.generate_gameobject("roadside",Vector2(top_right, dash_index - 5), Vector2(20, 20), (0,0,0))
        dash_index += 35
    
    dash_index = top_left + 10
    while dash_index < top_right:
        renderer.generate_gameobject("roadside",Vector2(dash_index - 5, bottom_left), Vector2(20, 20), (0,0,0))
        dash_index += 35

    dash_index = top_left + 10
    while dash_index < top_right:
        renderer.generate_gameobject("roadside",Vector2(dash_index - 5, bottom_right), Vector2(20, 20), (0,0,0))
        dash_index += 35
    
    renderer.generate_gameobject("roadside",Vector2(top_left + 5, bottom_left + 5), Vector2((top_right - top_left) / 1.02, (bottom_right - bottom_left) / 1.02), (0,0,0))

    renderer.generate_gameobject("traffic_light_1", Vector2(top_left - 200,bottom_left + 5), Vector2(200,bottom_right - bottom_left- 5), (200, 0, 0), 3)
    renderer.generate_gameobject("traffic_light_2", Vector2(top_right + 5 ,bottom_left + 5), Vector2(200,bottom_right - bottom_left- 5), (200, 0, 0), 4)
    renderer.generate_gameobject("traffic_light_3", Vector2(top_left + 5, bottom_left - 200), Vector2(top_right - top_left - 5,200), (200, 0, 0), 1)
    renderer.generate_gameobject("traffic_light_4", Vector2(top_left + 5, bottom_right + 5), Vector2(top_right - top_left - 5, 200), (200, 0, 0), 2)