import pgzero   
import random

from traffic_manager import TrafficManager

RED = 200, 0, 0
GREEN = 0, 200, 0
BLUE = 0, 0, 200
YELLOW = 200, 200, 0

from environment import *
from util import *

class GameObject:
    """This is going to essentially be the car class however we may use this as
       as an interface for the rest of the game world too.
    """
    def __init__(self, position, size, name, colour=None, traffic_light_seconds_init=None):
        self.coordinates = Vector2(position.x, position.y)
        self.size = size.return_tupled_data()
        self.render_area = Rect(self.coordinates.return_tupled_data(), self.size)
        self.name = name
        self.colour = colour
        self.current_impulse = Vector2(0,0)

        if False:
            if traffic_light_seconds_init is not None:
                self.traffic_light_timeout_ticks = 60 * traffic_light_seconds_init
                self.traffic_light_state = "Red"
                self.traffic_light_real_count_ticks = 0
                self.traffic_light_colours = ["Red", "Yellow", "Green"]
                self.traffic_light_stage_integer = 0

        self.traffic_light_state = "Red"
        self.traffic_light_real_count_ticks = 0
        self.traffic_light_colours = ["Red", "Yellow", "Green"]
        self.traffic_light_stage_integer = 0

        self.stopwatch_enabled = False
    
    def move_with_impulse(self, vector_impulse):
        """Move the chosen GameObject with an impulse
        
        Arguments:
            vector_impulse {Vector2} -- Moves in a direction
        """
        self.current_impulse = vector_impulse
        new_coords = self.coordinates.add(self.current_impulse)
        self.coordinates.x = new_coords[0]
        self.coordinates.y = new_coords[1]
        self.render_area = Rect(self.coordinates.return_tupled_data(), self.size)

    def change_colour(self, colour):
        self.colour = colour
    
    def move_absolute(self, vector_absolute):
        pass

    def tick_seconds(self):
        self.traffic_light_real_count_ticks += 1

    def decrease_seconds(self):
        self.traffic_light_real_count += 1

    def traffic_light_state_manager(self):
        print(self.traffic_light_real_count_ticks)
        self.traffic_light_real_count_ticks += 1
        if self.traffic_light_real_count_ticks > self.traffic_light_timeout_ticks:
            self.traffic_light_stage_integer += 1
            if self.traffic_light_stage_integer >= len(self.traffic_light_colours):
                self.traffic_light_stage_integer = 0
            self.traffic_light_real_count_ticks = 0
            self.traffic_light_state = self.traffic_light_colours[self.traffic_light_stage_integer]
            print(self.traffic_light_state)

    def update_colour(self):
        if self.traffic_light_state == "Red":
            self.change_colour(RED)
        if self.traffic_light_state == "Yellow":
            self.change_colour(YELLOW)
        if self.traffic_light_state == "Green":
            self.change_colour(GREEN)

    def stopwatch(self, seconds, start):
        if start:
            start = time.time()
        else:
            start = self.elapsed
        time.clock()
        self.elapsed = time.time() - start

class GraphicsManager:
    def __init__(self):
        self.gameobjects = []
        self.screen_dimensions = Vector2(1280, 720)
    
    def generate_gameobject(self, name, position, size, colour, traffic_light_seconds_init=None):
        """Generate a game object and put it into the draw list
        
        Arguments:
            name {name of object} -- name of object
            position {Vector2} -- Position to draw
            size {Vector2} -- size of rect
            colour {Colour} -- colour of rect we're drawing
        """
        gameobject = GameObject(position, size, name, colour, traffic_light_seconds_init=traffic_light_seconds_init)
        self.gameobjects.append(gameobject) 

    def get_gameobject_by_name(self, name):
        """Get the GameObjects we need based on the name of them
        
        Arguments:
            name {string} -- name that was given to the file
        
        Returns:
            GameObject -- entity that was searched for
        """
        temp_list = []
        for entity in self.gameobjects:
            if entity.name == name: 
                temp_list.append(entity)
        
        return temp_list

    def move_gameobject(self, name, impulse, limiter_x = -1, limiter_y = -1, limit_axis = None, tlstatus = None):
        """Move GameObjects with an impulse
        
        Arguments:
            name {string} -- Name of the object we're trying to move
            impulse {Vector2} -- Direction the impulse to move towards is
        """
        for entity in self.gameobjects:
            if entity.name == name:
                if limit_axis == None:
                    return
                elif limit_axis == "x_more":
                    if entity.coordinates.x > limiter_x:
                        entity.move_with_impulse(impulse)
                    elif tlstatus == "Green":
                        entity.move_with_impulse(impulse)
                elif limit_axis == "x_less":
                    if entity.coordinates.x < limiter_x:
                        entity.move_with_impulse(impulse)
                    elif tlstatus == "Green":
                        entity.move_with_impulse(impulse)
                elif limit_axis == "y_more":
                    if entity.coordinates.y > limiter_y:
                        entity.move_with_impulse(impulse)
                    elif tlstatus == "Green":
                        entity.move_with_impulse(impulse)
                elif limit_axis == "y_less":
                    if entity.coordinates.y < limiter_y:
                        entity.move_with_impulse(impulse)
                    elif tlstatus == "Green":
                        entity.move_with_impulse(impulse)


    def change_gameobject_colour(self, colour):
        for entity in self.gameobjects:
            if entity.name == name:
                entity.change_colour(colour)

    def draw_world(self):
        """draw all of the GameObjects that we want
        """
        screen.clear()
        for entity in self.gameobjects:
            screen.draw.filled_rect(entity.render_area, entity.colour)

render_window = GraphicsManager()
    
def junction_left_creation(start_spawn_position, spawn_end_point, middle_of_road, car_gap):
    i = start_spawn_position
    while i > spawn_end_point:
        render_window.generate_gameobject("left_car", Vector2(i,middle_of_road), car_left_right_rect, random.choice([BLUE]))
        i -= car_gap

def junction_right_creation(start_spawn_position, spawn_end_point, middle_of_road, car_gap):
    i = start_spawn_position
    while i < spawn_end_point:
        render_window.generate_gameobject("right_car", Vector2(i,middle_of_road), car_left_right_rect, (66,99,30))
        i += car_gap

def junction_up_creation(start_spawn_position, spawn_end_point, middle_of_road, car_gap):
    i = start_spawn_position
    while i > spawn_end_point:
        render_window.generate_gameobject("up_car", Vector2(middle_of_road,i), car_up_down_rect, (250,250,30))
        i -= car_gap

def junction_down_creation(start_spawn_position, spawn_end_point, middle_of_road, car_gap):
    i = start_spawn_position
    while i < spawn_end_point:
        render_window.generate_gameobject("down_car", Vector2(middle_of_road, i), car_up_down_rect, (250,250,230))
        i += car_gap

setup_scene(render_window)
tf1 = render_window.get_gameobject_by_name("traffic_light_1")[0]
tf2 = render_window.get_gameobject_by_name("traffic_light_2")[0]
tf3 = render_window.get_gameobject_by_name("traffic_light_3")[0]
tf4 = render_window.get_gameobject_by_name("traffic_light_4")[0]

junction_left_creation(400, -1000, 300, 50)
junction_right_creation(1000, 2600, 400, 100)
junction_up_creation(200, -8000, 700, 170)
junction_down_creation(500, 3000, 570, 40)

right_driving_impulse = Vector2(2, 0)
left_driving_impulse = Vector2(-2, 0)
up_driving_impulse = Vector2(0, -2)
down_driving_impulse = Vector2(0, 2)

stopped_impulse = Vector2(0,0)

left_cars = []
left_car_freq = 60
left_traffic_cars_moving = True

def left_road(impulse, seconds_frequency):
    global tf1
    active_cars = render_window.get_gameobject_by_name("left_car")
    result = active_cars[0].render_area.colliderect(tf1.render_area)
    # tf1.traffic_light_state_manager()

    if result and tf1.traffic_light_state == "Red" or result and tf1.traffic_light_state == "Yellow":
        current_impulse = stopped_impulse
    else:
        current_impulse = right_driving_impulse
    render_window.move_gameobject("left_car", current_impulse, limiter_x=400, limit_axis="x_more", tlstatus=tf1.traffic_light_state)

def right_road(impulse, seconds_frequency):
    global tf2
    active_cars = render_window.get_gameobject_by_name("right_car")
    result = active_cars[0].render_area.colliderect(tf2.render_area)
    # tf2.traffic_light_state_manager()

    if result and tf2.traffic_light_state == "Red" or result and tf2.traffic_light_state == "Yellow":
        current_impulse = stopped_impulse
    else:
        current_impulse = left_driving_impulse
    render_window.move_gameobject("right_car", current_impulse, limiter_x=800, limit_axis="x_less", tlstatus=tf2.traffic_light_state)

def up_road(impulse, seconds_frequency):
    global tf3
    active_cars = render_window.get_gameobject_by_name("up_car")
    result = active_cars[0].render_area.colliderect(tf3.render_area)
    # tf3.traffic_light_state_manager()

    if result and tf3.traffic_light_state == "Red" or result and tf3.traffic_light_state == "Yellow":
        current_impulse = stopped_impulse
    else:
        current_impulse = down_driving_impulse
    render_window.move_gameobject("up_car", current_impulse, limiter_y=300, limit_axis="y_more", tlstatus=tf3.traffic_light_state)


def down_road(impulse, seconds_frequency):
    global tf4
    active_cars = render_window.get_gameobject_by_name("down_car")
    result = active_cars[0].render_area.colliderect(tf4.render_area)
    # tf4.traffic_light_state_manager()

    if result and tf4.traffic_light_state == "Red" or result and tf4.traffic_light_state == "Yellow":
        current_impulse = stopped_impulse
    else:
        current_impulse = up_driving_impulse
    render_window.move_gameobject("down_car", current_impulse, limiter_y=480, limit_axis="y_less", tlstatus=tf4.traffic_light_state)

traffic_manager = TrafficManager()

traffic_manager.addTraffic(0,20)
traffic_manager.addTraffic(1,10)
traffic_manager.addTraffic(2,5)
traffic_manager.addTraffic(3,15)

ticks = 0
traffic_update_time = 30

def convert_numerical_to_string_tf_state(number):
    if number == 0:
        return "Red"
    if number == 1:
        return "Yellow"
    if number == 2:
        return "Green"
    if number == 3:
        return "Yellow"

def update_tf_states(tm):
    global tf1
    global tf2
    global tf3
    global tf4
    tf1.traffic_light_state = convert_numerical_to_string_tf_state(tm.junctionLightState[0])
    tf2.traffic_light_state = convert_numerical_to_string_tf_state(tm.junctionLightState[1])
    tf3.traffic_light_state = convert_numerical_to_string_tf_state(tm.junctionLightState[2])
    tf4.traffic_light_state = convert_numerical_to_string_tf_state(tm.junctionLightState[3])

    tf1.update_colour()
    tf2.update_colour()
    tf3.update_colour()
    tf4.update_colour()

times_done = 0
def intersection_logic():
    global traffic_manager
    global ticks
    global times_done
    ticks += 1
    if ticks > traffic_update_time:
        traffic_manager.update()
        update_tf_states(traffic_manager)
        ticks = 0
        times_done += 1

    left_road(None, None)
    right_road(None, None)
    up_road(None,None)
    down_road(None, None)

def draw():
    render_window.draw_world()

def update(dt):
    intersection_logic()
    