CONST_YELLOW_PERIOD = 1
CONST_GREEN_MIN_PERIOD = 1
CONST_GREEN_MAX_PERIOD = 10
CONST_NUMBER_OF_JUNCTIONS = 4
CONST_CAR_SPEED = 1

class TrafficManager:
    
    def __init__(self):
        print ("initialising")
        self.inputTraffic = [0, 0, 0, 0]
        self.junctionLightState = [1, 0, 0, 0]
        self.junctionLightGreenTime = [CONST_GREEN_MIN_PERIOD, CONST_GREEN_MIN_PERIOD, CONST_GREEN_MIN_PERIOD, CONST_GREEN_MIN_PERIOD]
        self.junctionLightYellowTime = CONST_YELLOW_PERIOD
        self.dirtyFirstRun = 0


    def update(self):
        print ("updating")
        for count in range (0, 4):
            if (self.dirtyFirstRun == 0):
                self.dirtyFirstRun == 1
                if (self.inputTraffic[count] <= CONST_GREEN_MAX_PERIOD):
                    self.junctionLightGreenTime[count] = self.inputTraffic[count]
                else:
                    self.junctionLightGreenTime[count] = CONST_GREEN_MAX_PERIOD
            
            if (self.junctionLightState[count] == 0):
                if (self.inputTraffic[count] <= CONST_GREEN_MAX_PERIOD):
                    self.junctionLightGreenTime[count] = self.inputTraffic[count]
                else:
                    self.junctionLightGreenTime[count] = CONST_GREEN_MAX_PERIOD
        
        self.updateJunctionLights()
        
        print('-                                  -')
        print('----------------(%d)----------------' % (self.inputTraffic[0]))
        print('-------------%d[[%d]]%d-------------' % (self.junctionLightState[0],self.junctionLightGreenTime[0],self.junctionLightState[0]))
        print('------------------------------------')
        print('--------%d----------------%d--------' % (self.junctionLightState[3],self.junctionLightState[1]))
        print('(%d)---[%d]--------------[%d]---(%d)' % (self.inputTraffic[3],self.junctionLightGreenTime[3],self.junctionLightGreenTime[1],self.inputTraffic[1]))
        print('--------%d----------------%d--------' % (self.junctionLightState[3],self.junctionLightState[1]))
        print('------------------------------------')
        print('-------------%d[[%d]]%d-------------' % (self.junctionLightState[2],self.junctionLightGreenTime[2],self.junctionLightState[2]))
        print('----------------(%d)----------------' % (self.inputTraffic[2]))
        print('-                                  -')


    def addTraffic(self, inputJunction, traffic):
        print ("adding traffic")

        if (inputJunction == 0):
            self.inputTraffic[0] += traffic
        if (inputJunction == 1):
            self.inputTraffic[1] += traffic
        if (inputJunction == 2):
            self.inputTraffic[2] += traffic
        if (inputJunction == 3):
            self.inputTraffic[3] += traffic
        if (inputJunction < 0) or (inputJunction > 3):
            print ("ERROR: UNKNOWN JUNCTION")


    def updateJunctionLights(self):
        print ("updating junction lights")

        if (self.junctionLightState.count(0) != 3):
            print ("ERROR: TOO FEW LIGHTS RED")
            return 
        
        if (self.junctionLightState.count(1) == 1):
            self.junctionLightYellowTime -= 1
            if (self.junctionLightYellowTime <= 0):
                self.junctionLightYellowTime == CONST_YELLOW_PERIOD
                
            self.junctionLightState[self.junctionLightState.index(1)] += 1
            return
        
        if (self.junctionLightState.count(2) == 1):
            self.junctionLightGreenTime[self.junctionLightState.index(2)] -= 1
            self.inputTraffic[self.junctionLightState.index(2)] -= CONST_CAR_SPEED 
            if (self.inputTraffic[self.junctionLightState.index(2)] < 0):
                self.inputTraffic[self.junctionLightState.index(2)] = 0

            if (self.junctionLightGreenTime[self.junctionLightState.index(2)] <= 0):
                self.junctionLightGreenTime[self.junctionLightState.index(2)] = CONST_GREEN_MIN_PERIOD
                
                self.junctionLightState[self.junctionLightState.index(2)] += 1
            return
        
        if (self.junctionLightState.count(3) == 1):
            self.junctionLightYellowTime -= 1
            if (self.junctionLightYellowTime <= 0):
                self.junctionLightYellowTime == CONST_YELLOW_PERIOD

                for x in range(0, 3):
                    if(self.inputTraffic[self.junctionLightState.index(3)+x] > 0 ):
                        self.junctionLightState[self.junctionLightState.index(3)+x] = 1
                        break
                self.junctionLightState[self.junctionLightState.index(3)] = 0
            return