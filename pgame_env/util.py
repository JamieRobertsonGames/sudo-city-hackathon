import pgzero
import random

RED = 200, 0, 0
GREEN = 0, 200, 0
BLUE = 0, 0, 200
YELLOW = 200, 200, 0

WIDTH = 1280
HEIGHT = 720

class Vector2():
    def __init__(self, x, y):
        """Generate a simple X Y vector
    
        Arguments:
        x {float} -- x coords
        y {float} -- y coords
        """
        self.x = x
        self.y = y

    def add(self, amount):
        """add some vectors together and return the result as a tuple
        
        Arguments:
            amount {Vector2} -- amount we're adding on
        
        Returns:
            tuple -- tuple of vectors (PGZero friendly)
        """
        x = self.x + amount.x
        y = self.y + amount.y
        return (x, y)

    def return_tupled_data(self):
        """Return PGZero friendly tuple data
        
        Returns:
            tuple -- x y in tuple form
        """
        return (self.x, self.y)
    
# Useful Vectors that I'll just wack here
car_up_down_rect = Vector2(5, 20)
car_left_right_rect = Vector2(20, 5)
